package com.sobel.android.hiddenbufferservice;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;

import android.util.Base64;

/**
 * Record class for keeping track of what queries have been run against
 * a particular hidden buffer
 * 
 * Immutable
 * Can be used as hashkey, set item, etc,
 * because hashCode and equals are implemented
 *
 */
public class HiddenBufferQuery {

	private final int mQueryType;
	private final byte[] mData; // never null

	private final int mHashCode;
	
	public HiddenBufferQuery(int queryType, byte[] data) {
		mQueryType = queryType;

		// Clone so immutable
		if (data == null) {
			mData = null;
		} else {
			mData = data.clone();
		}

		mHashCode = computeHashCode();
	}

	public int getQueryType() {
		return mQueryType;
	}

	public byte[] getData() {
		// Cloned to avoid receiever mutating
		return mData.clone();
	}

	private int computeHashCode() {
		// TODO:
		// Not sure if this is very good algorithm...
		int h = mQueryType;
		if (mData == null) {
			return h;
		}

		int i;
		for (i = 0; i < mData.length; i++) {
			h += mData[i];
		}
		return h;
	}
	
	public int hashCode() {
		return mHashCode;
	}

	private boolean dataEqual(byte[] a, byte[] b) {
		if (a == null) {
			return b == null;
		}
		if (b == null) {
			return false;
		}
		int l = a.length;
		if (b.length != l) {
			return false;
		}

		int i;
		for (i = 0; i < l;i++) {
			if (a[i] != b[i]) {
				return false;
			}
		}
		return true;
	}
	
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}

		HiddenBufferQuery other = (HiddenBufferQuery) obj;
		return (this.mQueryType == other.mQueryType && dataEqual(this.mData, other.mData));
	}
	
	/**
	 * [1 byte QT] [4 bytes BIG ENDIAN data length] [data*]
	 */
	public int serializedLength() {
		return 1 + 4 + (mData == null ? 0 : mData.length);
	}
	public byte[] serialize() {
		ByteBuffer bb = ByteBuffer.allocate(serializedLength());
		bb.put((byte)mQueryType);
		if (mData == null) {
			bb.putInt(0);
		} else {
			bb.putInt(mData.length);
			bb.put(mData);
		}
		return bb.array();
	}
	
	public String humanReadableString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[Query: ");
		sb.append(HiddenBufferService.queryTypeToString(mQueryType));
		
		switch (mQueryType) {
		case HiddenBufferService.REGEX_QUERY:
			sb.append(": ");
			try {
				sb.append(new String(mData, "UTF-8"));
			} catch (UnsupportedEncodingException e) {
				sb.append("<bad encoding>");
			}
			break;
		case HiddenBufferService.EBPF_QUERY:
			sb.append(": ");
			sb.append(Base64.encodeToString(mData, Base64.NO_WRAP));
		}

		sb.append("]");
		return sb.toString();
	}
}
