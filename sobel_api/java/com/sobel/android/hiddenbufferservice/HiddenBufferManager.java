package com.sobel.android.hiddenbufferservice;

import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;

public class HiddenBufferManager {

	IHiddenBufferService mHiddenBufferService;
	
	/**
	 * Constructor - Finds the HiddenBufferService
	 */
	public HiddenBufferManager() {
		IBinder hbsBinder = ServiceManager.getService("hidden_buffer");
        if (hbsBinder == null) {
        	throw new RuntimeException("No Such Service 'hidden_buffer'");
        } else {
        	mHiddenBufferService = IHiddenBufferService.Stub.asInterface(hbsBinder);
        }
	}
	
	/**
	 * Creates and returns handle to new hidden buffer
	 * @return
	 */
	public HiddenBufferHandle newBuffer() {
    	try {
    		return mHiddenBufferService.newBuffer();
    	} catch (RemoteException e) {
    		throw new RuntimeException("Error accessing Service: " + e.toString());
    	}
	}

	/**
	 * Sets the contents of a buffer
	 * requires privateToken
	 * returns whether the set actually happened
	 */
	public boolean setBufferText(IBinder privateToken, String s) {
		try {
			return mHiddenBufferService.setBufferText(privateToken, s);
		} catch (RemoteException e) {
			throw new RuntimeException("Error accessing Service: " + e.toString());
		}
	}
	
	/**
	 * Create a new snapshot.
	 * please free it when done :(
	 */
	public IBinder createSnapshot(IBinder publicToken) {
    	try {
    		return mHiddenBufferService.createSnapshot(publicToken);
    	} catch (RemoteException e) {
    		throw new RuntimeException("Error accessing Service: " + e.toString());
    	}
	}
	
	/**
	 * Frees a snapshot. THANKS!
	 */
	public boolean freeSnapshot(IBinder snapshotToken) {
    	try {
    		return mHiddenBufferService.freeSnapshot(snapshotToken);
    	} catch (RemoteException e) {
    		throw new RuntimeException("Error accessing Service: " + e.toString());
    	}
	}
	
	/**
	 * gets the contents of a snapshot.
	 * SYSTEM ONLY
	 * 
	 * @hide
	 */
	public String getContents(IBinder snapshotToken) {
    	try {
    		return mHiddenBufferService.getContents(snapshotToken);
    	} catch (RemoteException e) {
    		throw new RuntimeException("Error accessing Service: " + e.toString());
    	}
	}
	
	/**
	 * gets the log of the buffer of a snapshot
	 * SYSTEM ONLY
	 * 
	 * @hide
	 */
	public byte[] getLog(IBinder snapshotToken) {
    	try {
    		return mHiddenBufferService.getLog(snapshotToken);
    	} catch (RemoteException e) {
    		throw new RuntimeException("Error accessing Service: " + e.toString());
    	}
	}
	
	/**
	 * Test and set the exfiltrationState
	 * If not initialized or matches, sets it, sets allowQueries false, and returns true
	 * otherwise returns false
	 * 
	 * SYSTEM ONLY
	 * 
	 * @hide
	 */
	public boolean testAndSetExfiltrationTarget(IBinder snapshotToken, String target) {
		try {
			return mHiddenBufferService.testAndSetExfiltrationTarget(snapshotToken, target);
		} catch (RemoteException e) {
			throw new RuntimeException("Error accessing Service: " + e.toString());
		}
	}
	
	/**
	 * Set the exfiltration state
	 * 
	 * SYSTEM ONLY
	 * @hide
	 */
	public boolean exfiltrationAttemptFinished(IBinder snapshotToken, boolean lockdown) {
		try {
			return mHiddenBufferService.exfiltrationAttemptFinished(snapshotToken, lockdown);
		} catch (RemoteException e) {
			throw new RuntimeException("Error accessing Service: " + e.toString());
		}
	}
	
	
	/**
	 * Runs a query
	 * 
	 * @hide
	 */
	public HiddenBufferQueryResponse queryContent(IBinder publicToken, int queryType, byte[] data) {
		try {
			return mHiddenBufferService.queryContent(publicToken, queryType, data);
		} catch (RemoteException e) {
			throw new RuntimeException("Error accesing Service: " + e.toString());
		}
	}

}
