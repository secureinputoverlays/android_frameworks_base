package com.sobel.android.hiddenbufferservice;

import android.os.IBinder;
import com.sobel.android.hiddenbufferservice.HiddenBufferHandle;
import com.sobel.android.hiddenbufferservice.HiddenBufferQueryResponse;

/**
 * @hide
 */
interface IHiddenBufferService {

	HiddenBufferHandle newBuffer();
	boolean setBufferText(IBinder privateToken, String s);

	IBinder createSnapshot(IBinder publicToken);
	boolean freeSnapshot(IBinder snapshotToken);
	
	// These two need to be locked down to system only
	String getContents(IBinder snapshotToken);
	byte[] getLog(IBinder snapshotToken);

	// Exfiltration state -- these also need to be locked down.
	boolean testAndSetExfiltrationTarget(IBinder snapshotToken, String target);
	boolean exfiltrationAttemptFinished(IBinder snapshotToken, boolean lockdown);

	HiddenBufferQueryResponse queryContent(IBinder snapshotToken, int queryType, in byte[] data);
}
