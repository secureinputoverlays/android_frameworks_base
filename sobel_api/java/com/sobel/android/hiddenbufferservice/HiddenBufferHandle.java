package com.sobel.android.hiddenbufferservice;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;

public class HiddenBufferHandle implements Parcelable {

	public IBinder mPrivateToken; // This token gives read / write
	public IBinder mPublicToken;  // This token gives just query
	
	@Override
	public int describeContents() {
		return 0;
	}
	
	public HiddenBufferHandle(IBinder privateToken, IBinder publicToken) {
		mPrivateToken = privateToken;
		mPublicToken = publicToken;
	}

	public HiddenBufferHandle(Parcel in) {
		mPrivateToken = in.readStrongBinder();
		mPublicToken = in.readStrongBinder();
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeStrongBinder(mPrivateToken);
		dest.writeStrongBinder(mPublicToken);
	}
	
    public static final Parcelable.Creator<HiddenBufferHandle> CREATOR
    = new Parcelable.Creator<HiddenBufferHandle>() {
        public HiddenBufferHandle createFromParcel(Parcel in) {
            return new HiddenBufferHandle(in);
        }

        public HiddenBufferHandle[] newArray(int size) {
            return new HiddenBufferHandle[size];
        }
    };

    @Override
    public String toString() {
    	return String.format("HiddenBufferHandle<pri:%s,pub:%s>", mPrivateToken, mPublicToken);
    }

}
