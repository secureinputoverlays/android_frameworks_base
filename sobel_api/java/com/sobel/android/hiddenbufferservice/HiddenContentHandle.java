/**
 * User Facing handle to a particular hidden buffer
 */

package com.sobel.android.hiddenbufferservice;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.regex.Pattern;

import android.os.IBinder;

public class HiddenContentHandle {
	
	public static class QueryError extends RuntimeException{
		public QueryError(String s) {
			super(s);
		}
	}

	private IBinder mSnapshotToken;
	private HiddenBufferManager mHiddenBufferManager;

	// Track if we've been freed.
	// RuntimeError if we have
	private boolean mFreed;

	/**
	 * TODO: rehide this
	 */
	public HiddenContentHandle(IBinder snapshotToken) {
		mSnapshotToken = snapshotToken;
		mHiddenBufferManager = new HiddenBufferManager();

		mFreed = false;
	}

	/**
	 * Call this to deallocate the buffer.
	 * RuntimeException if called multiple times (being strict).
	 */
	public void free() {
		if (mFreed) {
			throw new RuntimeException("Already been freed!");
		}
		// Actually do it now...
		mHiddenBufferManager.freeSnapshot(mSnapshotToken);
		mFreed = true;
	}

	public boolean hasBeenFreed() {
		return mFreed;
	}

	/**
	 * @hide
	 */
	public IBinder getSnapshotToken() {
		if (hasBeenFreed()) {
			throw new RuntimeException("Cannot use after free");
		}
		return mSnapshotToken;
	}
	
	/**
	 * Throws runtime exception if we've been freed.
	 */
	private HiddenBufferQueryResponse doQuery(int queryType, byte[] data) {
		if (hasBeenFreed()) {
			throw new RuntimeException("Cannot query after free");
		}

		HiddenBufferQueryResponse r = mHiddenBufferManager.queryContent(
				mSnapshotToken,
				queryType,
				data);
		if (r == null) {
			throw new QueryError("Unknown error: null response");
		}
		if (r.isError()) {
			throw new QueryError(r.getErrorMessage());
		}
		return r;
	}
	
	public int getLength() {
		HiddenBufferQueryResponse r = doQuery(HiddenBufferService.LENGTH_QUERY, null);
		return r.getIntResult();
	}

	public boolean matchesPattern(Pattern p) {
		byte[] data;
		try {
			 data = p.toString().getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw new QueryError("Error encoding pattern");
		}
		// [ flags | data...]
		ByteBuffer bb = ByteBuffer.allocate(4 + data.length);
		bb.putInt(p.flags());
		bb.put(data);
		HiddenBufferQueryResponse r = doQuery(HiddenBufferService.REGEX_QUERY, bb.array());
		return r.getBoolResult();
	}

	public int runEBPF(byte[] code) {
		// Input is code to run... not using
		// EBPFInstruction because this way code could
		// come from any source... (conceivably)
		// That said, we could do some sanity checks here,
		// but why bother?
		HiddenBufferQueryResponse r = doQuery(HiddenBufferService.EBPF_QUERY, code);
		return r.getIntResult();
	}
}
