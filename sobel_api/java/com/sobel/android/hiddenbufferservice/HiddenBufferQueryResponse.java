package com.sobel.android.hiddenbufferservice;

import android.os.Parcel;
import android.os.Parcelable;

/** 
 *  Union type for passing back from hidden buffer query

 *  
 *  @hide
 */
public class HiddenBufferQueryResponse implements Parcelable {

	private final int mQueryType;

	private final boolean mError;
	private final String mErrorMessage;
	
	private final int mIntResult;
	private final boolean mBoolResult;
	
	@Override
	public int describeContents() {
		return 0;
	}
	
	public static HiddenBufferQueryResponse makeErrorResponse(String s) {
		return new HiddenBufferQueryResponse(-1, true, s, 0, false);
	}
	
	public static HiddenBufferQueryResponse makeLengthResponse(int l) {
		return new HiddenBufferQueryResponse(
				HiddenBufferService.LENGTH_QUERY,
				false,
				null,
				l,
				false);
	}
	
	public static HiddenBufferQueryResponse makeRegexResponse(boolean m) {
		return new HiddenBufferQueryResponse(
				HiddenBufferService.REGEX_QUERY,
				false,
				null,
				0,
				m);
	}
	
	public static HiddenBufferQueryResponse makeEbpfResponse(int result) {
		return new HiddenBufferQueryResponse(
				HiddenBufferService.EBPF_QUERY,
				false,
				null,
				result,
				false);
	}
	
	private HiddenBufferQueryResponse(int qt, boolean err, String msg, int i, boolean b) {
		mQueryType = qt;
		mError = err;
		mErrorMessage = msg;
		mIntResult = i;
		mBoolResult = b;

		// Throws runtime error if not OK.
		consistencyCheck();
	}
	
	private void consistencyCheck() {
		if (mError) {
			if (mErrorMessage == null) {
				throw new RuntimeException("Missing Error Message");
			}
			return;
		}
		if (mErrorMessage != null) {
			throw new RuntimeException("Unexpected Error Message");
		}

		if (!HiddenBufferService.validQueryType(mQueryType)) {
			throw new RuntimeException(String.format("Bad QueryType!: %d", mQueryType));
		}
	}

	public static HiddenBufferQueryResponse fromParcel(Parcel in) {
		int qt = in.readInt();
		boolean err = (in.readByte() != 0);
		String msg = in.readString();
		int i = in.readInt();
		boolean b = (in.readByte() != 0);
		return new HiddenBufferQueryResponse(qt, err, msg, i, b);
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(mQueryType);
		dest.writeByte((byte)(mError ? 1 : 0));
		dest.writeString(mErrorMessage);
		dest.writeInt(mIntResult);
		dest.writeByte((byte)(mBoolResult ? 1 : 0));
	}
	
    public static final Parcelable.Creator<HiddenBufferQueryResponse> CREATOR
    = new Parcelable.Creator<HiddenBufferQueryResponse>() {
        public HiddenBufferQueryResponse createFromParcel(Parcel in) {
            return HiddenBufferQueryResponse.fromParcel(in);
        }

        public HiddenBufferQueryResponse[] newArray(int size) {
            return new HiddenBufferQueryResponse[size];
        }
    };

	public boolean isError() {
		return mError;
	}

	public String getErrorMessage() {
		if (!mError) {
			throw new RuntimeException("Get error message but not error response");
		}
		return mErrorMessage;
	}

	public int getIntResult() {
		if (mQueryType != HiddenBufferService.LENGTH_QUERY && mQueryType != HiddenBufferService.EBPF_QUERY) {
			throw new RuntimeException("Get int result but not an int returning query");
		}
		return mIntResult;
	}

	public boolean getBoolResult() {
		if (mQueryType != HiddenBufferService.REGEX_QUERY) {
			throw new RuntimeException("Get bool result but not a bool returning query");
		}
		return mBoolResult;
	}

}
