package com.sobel.android.hiddenbufferservice;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import com.sobel.android.secureinputservice.SecureInputWindow;
import com.sobel.jebpf.EBPFInstruction;
import com.sobel.jebpf.EBPFInstruction.EBPFDecodeException;
import com.sobel.jebpf.EBPFInterpreter;
import com.sobel.jebpf.EBPFInterpreter.EBPFProgramException;

import android.app.ActivityThread;
import android.content.Context;
import android.graphics.PixelFormat;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Process;
import android.os.RemoteException;
import android.text.InputType;
import android.util.Log;
import android.util.Slog;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

/**
 * 
 * Hidden buffer service
 * 
 * creation and access of hidden buffers
 * each hidden buffer has two tokens:
 *  - a private token, which the creator gets. This token
 *    allows full write and read
 *  - a public token, which only allows querying
 *    everything done with a public token is logged
 * 
 * @hide
 */
public class HiddenBufferService extends IHiddenBufferService.Stub {

	private static final String TAG = "HiddenBufferService";

	public static final int LENGTH_QUERY = 0;
	public static final int REGEX_QUERY = 1;
	public static final int EBPF_QUERY = 2;
	
	// Set to true to allow applications to create hidden buffers
	// and access their contents at will. Only point of this is for
	// benchmark application, really.
	private static final boolean MAKE_HIDDEN_BUFFER_SERVICE_INSECURE = false;
	
	// Share the same values, the string contents accessed by reference
	private ConcurrentHashMap<IBinder, HiddenBuffer> mBuffersByPrivateToken;
	private ConcurrentHashMap<IBinder, HiddenBuffer> mBuffersByPublicToken;

	private ConcurrentHashMap<IBinder, BufferSnapshot> mSnapshots;

	public HiddenBufferService() {
		mBuffersByPrivateToken = new ConcurrentHashMap<IBinder, HiddenBuffer>();
		mBuffersByPublicToken = new ConcurrentHashMap<IBinder, HiddenBuffer>();

		mSnapshots = new ConcurrentHashMap<IBinder, BufferSnapshot>();
	}
	
	private void assertSystemCaller(String method) {
		if (MAKE_HIDDEN_BUFFER_SERVICE_INSECURE) {
			return;
		}
        int callingUid = Binder.getCallingUid();
        if (callingUid != Process.SYSTEM_UID) {
            throw new SecurityException("HiddenBufferService:" + method + " can only be run by the system");
        }
	}

	private boolean isPublicToken(IBinder t) {
		return mBuffersByPublicToken.containsKey(t);
	}
	private boolean isPrivateToken(IBinder t) {
		return mBuffersByPrivateToken.containsKey(t);
	}
	
	private synchronized HiddenBufferHandle createNewBuffer() {
		Binder privateToken = new Binder();
		Binder publicToken = new Binder();
		HiddenBufferHandle h = new HiddenBufferHandle(privateToken, publicToken);

		// Synchronized so this is OK.
		HiddenBuffer b = new HiddenBuffer(h.mPrivateToken, h.mPublicToken);
		mBuffersByPrivateToken.put(h.mPrivateToken, b);
		mBuffersByPublicToken.put(h.mPublicToken, b);
		return h;
	}
	
	@Override
	public HiddenBufferHandle newBuffer() throws RemoteException {
		assertSystemCaller("newBuffer");

		// For now, just stick in a UUID
		HiddenBufferHandle h = createNewBuffer();
		Log.i(TAG, "Created new hidden buffer, handle:" + h.toString());
		return h;
	}
	
	/**
	 * Requires privateToken permissions
	 */
	@Override
	public boolean setBufferText(IBinder privateToken, String s) throws RemoteException {
		if (!isPrivateToken(privateToken)) {
			Log.v(TAG, String.format("setBufferText: %s not a private token", privateToken));
			return false;
		}
		HiddenBuffer b = mBuffersByPrivateToken.get(privateToken);
		// Even though isPrivateToken checks, race condition with delete
		if (b == null) {
			Log.v(TAG, String.format("setBufferText: no buffer associated with %s", privateToken));
			return false;
		}
		synchronized (b) {
			// Do an integrity assertion,,, defensive
			if (b.mPrivateToken != privateToken) {
				throw new RuntimeException(String.format("Integrity failure! (%s, %s)", b.mPrivateToken, privateToken));
			}
			Log.v(TAG, String.format("setBufferText: setting buffer %s to %s", privateToken, s));
			b.mString = s;
		}
		return true;
	}
	
	@Override
	public synchronized IBinder createSnapshot(IBinder publicToken) throws RemoteException {
		// Check that public token is valid.
		// Grab the content and make a snapshot!
		// Check if valid token
		if (!isPublicToken(publicToken)) {
			Log.v(TAG, String.format("createSnapshot: %s not a public token", publicToken));
			return null;
		}

		HiddenBuffer b = mBuffersByPublicToken.get(publicToken);
		// Even though isPublicToken checks, race condition with delete
		if (b == null) {
			Log.v(TAG, String.format("createSnapshot: no buffer associated with %s", publicToken));
			return null;
		}

		BufferSnapshot ss = new BufferSnapshot(b.mString, b.mPublicToken);
		IBinder snapshotToken = new Binder();
		mSnapshots.put(snapshotToken , ss);
		Log.v(TAG, String.format("createSnapshot: created snapshot %s", snapshotToken ));
		return snapshotToken ;
	}
	
	@Override
	public synchronized boolean freeSnapshot(IBinder snapshotToken) throws RemoteException {
		BufferSnapshot snapshot = mSnapshots.get(snapshotToken);
		if (snapshot == null) {
			Log.v(TAG, String.format("freeSnapshot: no snapshot assocaiated with %s", snapshotToken));
			return false;
		}
		mSnapshots.remove(snapshotToken);
		Log.v(TAG, String.format("freeSnapshot: freed snapshot %s", snapshotToken));
		return true;
	}
	

	@Override
	public String getContents(IBinder snapshotToken) throws RemoteException {
		assertSystemCaller("getContents");

		// Concurrent hashmap so OK.
		BufferSnapshot snapshot = mSnapshots.get(snapshotToken);
		if (snapshot == null) {
			Log.v(TAG, String.format("getContents: no snapshot assocaiated with %s", snapshotToken));
			return null;
		}
		Log.v(TAG, String.format("getContents: returning %s for %s", snapshot.mString, snapshotToken));
		return snapshot.mString;
	}

	@Override
	public byte[] getLog(IBinder snapshotToken) throws RemoteException {
		assertSystemCaller("getLog");

		// Concurrent hashmap so OK.
		BufferSnapshot snapshot = mSnapshots.get(snapshotToken);
		if (snapshot == null) {
			Log.v(TAG, String.format("getLog: no snapshot assocaiated with %s", snapshotToken));
			return null;
		}
		// Resolve the public token.
		IBinder publicToken = snapshot.mPublicToken;
		// Check if valid token
		if (!isPublicToken(publicToken)) {
			Log.v(TAG, String.format("getLog: snapshots public token %s is not", publicToken));
			return null;
		}

		// We need the buffer to log the query!
		HiddenBuffer b = mBuffersByPublicToken.get(publicToken);

		// Even though isPublicToken checks, race condition with delete
		if (b == null) {
			Log.v(TAG, String.format("getLog: no buffer associated with %s", publicToken));
			return null;
		}
		return b.serializedLog();
	}
	
	@Override
	/**
	 * Exfiltration target
	 * look up the snapshot's buffer,
	 * lock it, then do test and set
	 * 
	 * If exfiltrationTarget is currently null, set it to target, lock queries, and return true
	 * Otherwise, if it same as passed target, lock queries, and return true
	 * Else, return false
	 * 
	 * this `starts` an exfiltration attempt
	 */
	public boolean testAndSetExfiltrationTarget(IBinder snapshotToken, String target) throws RemoteException {
		assertSystemCaller("testAndSetExfiltrationTarget");

		// Concurrent hashmap so OK.
		BufferSnapshot snapshot = mSnapshots.get(snapshotToken);
		if (snapshot == null) {
			Log.v(TAG, String.format("testAndSetExfiltrationTarget: no snapshot assocaiated with %s", snapshotToken));
			return false;
		}
		// Resolve the public token.
		IBinder publicToken = snapshot.mPublicToken;
		// Check if valid token
		if (!isPublicToken(publicToken)) {
			Log.v(TAG, String.format("testAndSetExfiltrationTarget: snapshots public token %s is not", publicToken));
			return false;
		}

		// We need the buffer to log the query!
		HiddenBuffer b = mBuffersByPublicToken.get(publicToken);

		// Even though isPublicToken checks, race condition with delete
		if (b == null) {
			Log.v(TAG, String.format("testAndSetExfiltrationTarget: no buffer associated with %s", publicToken));
			return false;
		}

		// Ok, now lock b and do the testandset
		synchronized (b) {
			if (b.mExfiltrationTarget == null) {
				b.mExfiltrationTarget = target;
				b.mExfiltrationInProgress = true;
				return true;
			} else {
				if (b.mExfiltrationTarget.equals(target)) {
					b.mExfiltrationInProgress = true;
					return true;
				} else {
					return false;
				}
			}
		}
	}
	
	@Override
	/**
	 * Resets exfiltration state.
	 * Lockdown turns on locking down buffer (preventing future quereis)
	 * but cannot turn it off (|= with foo)
	 * A noop if no exfiltration in progress
	 */
	public boolean exfiltrationAttemptFinished(IBinder snapshotToken, boolean lockdown) throws RemoteException {
		assertSystemCaller("exfiltrationAttemptFinished");

		// Concurrent hashmap so OK.
		BufferSnapshot snapshot = mSnapshots.get(snapshotToken);
		if (snapshot == null) {
			Log.v(TAG, String.format("exfiltrationAttemptFinished: no snapshot assocaiated with %s", snapshotToken));
			return false;
		}
		// Resolve the public token.
		IBinder publicToken = snapshot.mPublicToken;
		// Check if valid token
		if (!isPublicToken(publicToken)) {
			Log.v(TAG, String.format("exfiltrationAttemptFinished: snapshots public token %s is not", publicToken));
			return false;
		}

		// We need the buffer to log the query!
		HiddenBuffer b = mBuffersByPublicToken.get(publicToken);

		// Even though isPublicToken checks, race condition with delete
		if (b == null) {
			Log.v(TAG, String.format("exfiltrationAttemptFinished: no buffer associated with %s", publicToken));
			return false;
		}
		
		// Does this have to be synchronized?
		synchronized (b) {
			if (!b.mExfiltrationInProgress) {
				// Fail loudly
				Log.v(TAG, String.format("exfiltrationAttemptFinished: %s not exfiltration in progress..", publicToken));
				return false;
			}
			b.mExfiltrationInProgress = false;
			b.mExfiltrationSuccess |= lockdown;
		}
		return true;
	}
	
	public static boolean validQueryType(int qt) {
		return qt == LENGTH_QUERY || qt == REGEX_QUERY || qt == EBPF_QUERY;
	}
	
	public static String queryTypeToString(int qt) {
		switch (qt) {
		case LENGTH_QUERY:
			return "LENGTH";
		case REGEX_QUERY:
			return "REGEX";
		case EBPF_QUERY:
			return "EBPF";
		default:
			return "[UNKNOWN]";
		}
	}
	
	/**
	 * Query over the buffer using public token
	 */
	@Override
	public HiddenBufferQueryResponse queryContent(IBinder snapshotToken,
			int queryType, byte[] data) throws RemoteException {

		if (!validQueryType(queryType)) {
			return HiddenBufferQueryResponse.makeErrorResponse("Invalid Query Type");
		}

		// Ensure there is a valid corresponding snapshot.
		// mSnapshots is concurrent hash map so OK
		BufferSnapshot snapshot = mSnapshots.get(snapshotToken);
		if (snapshot == null) {
			Log.v(TAG, String.format("queryContent: %s not a snapshot token", snapshotToken));
		}

		IBinder publicToken = snapshot.mPublicToken;
		// Check if valid token
		if (!isPublicToken(publicToken)) {
			Log.v(TAG, String.format("queryContent: snapshots public token %s is not", publicToken));
			return HiddenBufferQueryResponse.makeErrorResponse("Invalid Token");
		}

		// We need the buffer to log the query!
		HiddenBuffer b = mBuffersByPublicToken.get(publicToken);

		// Even though isPublicToken checks, race condition with delete
		if (b == null) {
			Log.v(TAG, String.format("queryContent: no buffer associated with %s", publicToken));
			return HiddenBufferQueryResponse.makeErrorResponse("Invalid Token");
		}

		synchronized (b) {
			// Do integrity assertion
			if (b.mPublicToken != publicToken) {
				throw new RuntimeException(String.format("Integrity failure! (%s, %s)", b.mPublicToken, publicToken));
			}
		}
		
		// Assert that querying is currently allowed on this buffer
		// We could be a __little__ nicer and just enforce that the query
		// has _already_ been run, but why bother? because it simplifies things!
		if (!b.queryAllowed(queryType, data)) {
			return HiddenBufferQueryResponse.makeErrorResponse("Query Not Permitted on Buffer. Maybe already exfiltrated?");
		}
		
		// Pull out a reference to the snapshot contents.
		String currentContents = snapshot.mString;

		// MMMk.
		switch (queryType) {
		case LENGTH_QUERY:
			b.logQuery(LENGTH_QUERY, null);
			int l = currentContents.length();
			return HiddenBufferQueryResponse.makeLengthResponse(l);
		case REGEX_QUERY:
			// Try and turn bytes back into string, then into pattern.
			if (data == null) {
				return HiddenBufferQueryResponse.makeErrorResponse("Data must not be null");
			}
			if (data.length < 4) {
				return HiddenBufferQueryResponse.makeErrorResponse("Data must have int flags first 4 bytes");
			}
			ByteBuffer patternBb = ByteBuffer.wrap(data);
			int flags = patternBb.getInt();
			byte[] patternBytes = new byte[data.length - 4];
			patternBb.get(patternBytes);

			String patternString;
			try {
				patternString = new String(patternBytes, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				return HiddenBufferQueryResponse.makeErrorResponse("Error decoding pattern string");
			}
			
			Pattern pattern;
			try {
				pattern = Pattern.compile(patternString, flags);
			} catch (PatternSyntaxException e) {
				return HiddenBufferQueryResponse.makeErrorResponse("Regex syntax error");
			}

			// Ok, now the query is going to happen, so log it.
			b.logQuery(REGEX_QUERY, data);
			Matcher m = pattern.matcher(currentContents);
			boolean match = m.matches();
			return HiddenBufferQueryResponse.makeRegexResponse(match);
		default:
			EBPFInstruction[] code;
			try {
				code = EBPFInstruction.decodeMany(data);
			} catch (EBPFDecodeException e) {
				return HiddenBufferQueryResponse.makeErrorResponse("Error parsing EBPF code: " + e.getMessage());
			}
			
			// Pack our "packet".
			byte[] currentContentBytes;
			try {
				currentContentBytes = currentContents.getBytes("UTF-8");
			} catch (UnsupportedEncodingException e1) {
				return HiddenBufferQueryResponse.makeErrorResponse("Error encoding buffer contents");
			} 
			ByteBuffer ebpfBb = ByteBuffer.allocate(4 + currentContentBytes.length);
			ebpfBb.putInt(currentContentBytes.length);
			ebpfBb.put(currentContentBytes);
			
			// Log it now - yes, there could be an error,
			// but user could choose the error based on packet data
			// So after this point is when we leak information.
			b.logQuery(EBPF_QUERY, data);

			int result;
			EBPFInterpreter t = new EBPFInterpreter(code);
			try {
				result = t.run(ebpfBb.array());
			} catch (EBPFProgramException e) {
				return HiddenBufferQueryResponse.makeErrorResponse("Error running EBPF code: " + e.getMessage());
			}
			return HiddenBufferQueryResponse.makeEbpfResponse(result);
		}
		
	}
	
	/**
	 * Structure representing the buffer
	 */
	private static class HiddenBuffer {
		public String mString;
		public final IBinder mPrivateToken;
		public final IBinder mPublicToken;

		public final HashSet<HiddenBufferQuery> mQuerySet;
		
		// Exfiltration state
		public String mExfiltrationTarget;
		public boolean mExfiltrationInProgress;
		public boolean mExfiltrationSuccess;
		
		public HiddenBuffer(IBinder privateToken, IBinder publicToken) {
			mString = "";
			mPrivateToken = privateToken;
			mPublicToken = publicToken;

			mQuerySet = new HashSet<HiddenBufferQuery>();
			
			mExfiltrationTarget = null;
			mExfiltrationInProgress = false;
			mExfiltrationSuccess = false;
		}
		
		public boolean queryAllowed(int queryType, byte[] data) {
			// If it's locked or possible already exfiltrated,
			// only allowed if already been done
			if (mExfiltrationInProgress || mExfiltrationSuccess) {
				HiddenBufferQuery q = new HiddenBufferQuery(queryType, data);
				synchronized (mQuerySet) {
					return mQuerySet.contains(q);
				}
			} else {
				return true;
			}
		}
		
		public byte[] serializedLog() {
			synchronized(mQuerySet) {
				int i = mQuerySet.size();

				// Compute length
				int length = 0;
				Iterator<HiddenBufferQuery> it1 = mQuerySet.iterator();
				while (it1.hasNext()) {
					length += it1.next().serializedLength();
				}

				ByteBuffer bb = ByteBuffer.allocate(length);
				Iterator<HiddenBufferQuery> it2 = mQuerySet.iterator();
				while (it2.hasNext()) {
					bb.put(it2.next().serialize());
				}
				return bb.array();
			}
		}
		
		private void logQuery(int queryType, byte[] data) {
			HiddenBufferQuery q = new HiddenBufferQuery(queryType, data);
			synchronized(mQuerySet) {
				if (!mQuerySet.contains(q)) {
					Log.v(TAG, String.format("Adding %s to query set for %s", q.humanReadableString(), mPublicToken));
				}
				mQuerySet.add(q);
			}
		}
	}
	
	/**
	 * Structure for a snapshot
	 */
	private static class BufferSnapshot {
		public final String mString;
		public final IBinder mPublicToken;

		public BufferSnapshot(String s, IBinder t) {
			mString = s;
			mPublicToken = t;
		}
	}


}
