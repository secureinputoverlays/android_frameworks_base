/**
 * Interface for user to receive events
 * from a SecureInputView
 */

package com.sobel.android.secureinputservice;

public interface SecureInputViewListener {
	public void onEditStart();
	public void onEditStop();
}
