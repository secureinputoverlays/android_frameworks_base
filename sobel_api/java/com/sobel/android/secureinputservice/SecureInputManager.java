package com.sobel.android.secureinputservice;

import java.util.HashMap;
import java.util.Iterator;

import android.content.Context;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;
import android.widget.TextView;

public class SecureInputManager {

	private static final String TAG = "SecureInputManager";

	static SecureInputManager sInstance;
	
	private final ISecureInputService mSecureInputService;
	private final ISecureInputServiceListener mSecureInputServiceListener;

	private enum InputStateEnum {
		STATE_EDITING,
		STATE_NOT_EDITING
	}
	
	/**
	 * A record.
	 * Keep one of these around per secure input window
	 */
	private static class SecureInputState {
		public SecureInputView inputView;
		public InputStateEnum state;
		
		public SecureInputState (SecureInputView inputView, InputStateEnum state) {
			this.inputView = inputView;
			this.state = state;
		}
	
	}
	
	private final HashMap<IBinder, SecureInputState> mStateByWindowId;

	private final static int REMOTE_WINDOW_EDIT_START = 1;
	private final static int REMOTE_WINDOW_EDIT_STOP = 2;

    /**
     * Retrieve the global SecureInputManager instance, creating it if it
     * doesn't already exist.
     * 
     * @hide
     */
    public static SecureInputManager getInstance() {
        synchronized (SecureInputManager.class) {
            if (sInstance == null) {
                IBinder b = ServiceManager.getService(Context.SECURE_INPUT_SERVICE);
                if (b == null) {
                	throw new RuntimeException("No Such Service 'secure_input'");
                }
                ISecureInputService service = ISecureInputService.Stub.asInterface(b);
                
                // TODO: InputMethodManager passes the Looper.getMainLooper as an arg.
                //		 Not sure why... not copying though.
                sInstance = new SecureInputManager(service);
            }
            return sInstance;
        }
    }
	
	/**
	 * Constructor - Finds the SecureInputService
	 */
	SecureInputManager(ISecureInputService service) {
        mSecureInputService = service;
        mSecureInputServiceListener = new SecureInputServiceListener();
        mStateByWindowId = new HashMap<IBinder, SecureInputState>();
        mHandler = new MainHandler(Looper.getMainLooper());
	}
	
	/**
	 * @hide
	 */
	public SecureInputHandle addWindow(String content, IBinder appToken, int w, int x, int y, int inputType, SecureInputView siv) {
		try {
			SecureInputHandle h = mSecureInputService.addWindow(content, appToken, w, x, y, inputType, mSecureInputServiceListener);
			SecureInputState state = new SecureInputState(siv, InputStateEnum.STATE_NOT_EDITING);
			mStateByWindowId.put(h.mSecureInputToken, state);
			return h;
		} catch (RemoteException e) {
			throw new RuntimeException("Error accessing Service: " + e.toString());
		}
	}
	

	private void startEditing(IBinder windowId) {
		// Adjust the state
		// TODO: check?
		SecureInputState s = mStateByWindowId.get(windowId);

		if (s.state != InputStateEnum.STATE_EDITING) {
			// Send this message
			s.state = InputStateEnum.STATE_EDITING;
			Message msg = mHandler.obtainMessage(REMOTE_WINDOW_EDIT_START, windowId);
			mHandler.sendMessage(msg);	
		}
	}
	
	private void stopEditing(IBinder windowId) {
		// Adjust state
		// TODO: check?
		SecureInputState s = mStateByWindowId.get(windowId);
		if (s.state != InputStateEnum.STATE_NOT_EDITING) {
			s.state = InputStateEnum.STATE_NOT_EDITING;
			Message msg = mHandler.obtainMessage(REMOTE_WINDOW_EDIT_STOP, windowId);
			mHandler.sendMessage(msg);
		}
	}
	
	private final class SecureInputServiceListener extends ISecureInputServiceListener.Stub {
		// This is not on UI thread, but rather a binder thread.
		@Override
		public void onEditStart(IBinder windowId) throws RemoteException {
			Log.v(TAG, String.format("Got edit start event for windowId %s", windowId));
			startEditing(windowId);
			
			// Now we also have to check for any _other_ windows
			// that think the're being edited, so that we can stop them!
			Iterator<IBinder> it = mStateByWindowId.keySet().iterator();
			while (it.hasNext()) {
				IBinder thisWindowId = it.next();
				if (windowId != thisWindowId) {
					stopEditing(thisWindowId);
				}
			}
		}

		@Override
		public void onEditEnd(IBinder windowId) throws RemoteException {
			Log.v(TAG, String.format("Got edit end event for windowId %s", windowId));
			stopEditing(windowId);
		}
		
	}
	
	private final MainHandler mHandler;
	private final class MainHandler extends Handler {
		
		public MainHandler(Looper l) {
			super(l);
		}
		
		@Override
		public void handleMessage(Message msg) {
			IBinder windowId;
			switch(msg.what) {
			case REMOTE_WINDOW_EDIT_START:
				windowId = (IBinder)msg.obj;
				// TODO: checking?
				mStateByWindowId.get(windowId).inputView.onEditStart();
				break;
			case REMOTE_WINDOW_EDIT_STOP:
				windowId = (IBinder)msg.obj;
				// TODO: checking?
				mStateByWindowId.get(windowId).inputView.onEditStop();
				break;
			default:
				throw new RuntimeException(String.format("Unknown SecureInputManager.MainHandler msg: %d", msg.what));
			}
		}
		
	}
}
