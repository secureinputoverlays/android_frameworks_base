package com.sobel.android.secureinputservice;

import android.os.IBinder;

import com.sobel.android.secureinputservice.SecureInputHandle;
import com.sobel.android.secureinputservice.ISecureInputServiceListener;

/** @hide */
interface ISecureInputService {

	SecureInputHandle addWindow(String content, IBinder app, int w, int x, int y, int inputType, ISecureInputServiceListener l);
	
}
