package com.sobel.android.secureinputservice;

import android.content.Context;
import android.graphics.PixelFormat;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.WindowManager;
import android.widget.EditText;

/**
 * Implements a Secure Input Window and associated logic
 * 
 * @hide
 */
public class SecureInputWindow {

	private final static String TAG = "SecureInputWindow";
	
	private final static boolean DEBUG = true;
	
	private String mTag;
	
	private Context mContext;
	private Handler mHandler;
	private IBinder mParentToken;

	private SecureInputWindowListener mListener;

	private WindowManager mWindowManager;
	private EditText mTv;
	private WindowManager.LayoutParams mParams;

	private int mWidth;
	
	public IBinder windowId;
	
	public SecureInputWindow(int w, Context c, Handler h, IBinder t, String contents, int inputType, SecureInputWindowListener l, String tag) {
		mTag = tag;

		mContext = c;
		mHandler = h;
		mParentToken = t;

		mListener = l;

		mWidth = w;
		
		mWindowManager = (WindowManager)c.getSystemService(Context.WINDOW_SERVICE);
		
		mTv = new EditText(mContext);
		mTv.setText(contents);

		// Disable multi line
		inputType &= (~InputType.TYPE_TEXT_FLAG_MULTI_LINE);
		
    	// Disable spell checking
    	// Because that results in editor trying to open a popup as a sub-subwindow,
    	// Which causes zygote-level crash :o
    	inputType |= InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS;

    	Log.v(TAG, String.format("Input type: %x", inputType));
    	mTv.setInputType(inputType);

    	mTv.setOnTouchListener(new WindowViewTouchListener());
    	mTv.addTextChangedListener(new WindowTextViewWatcher());
    	mTv.setOnKeyListener(new WindowKeyListener());

    	mParams = getInitialLayoutParams();
    	mParams.token = mParentToken;

    	windowId = new Binder();
	}
	
	public void show(final int x, final int y) {
		mParams.x = x;
		mParams.y = y;
    	mHandler.post(new Runnable() {
			@Override
			public void run() {
				if (DEBUG) Log.i(TAG, "Adding " + mTag + " at " + x + "," + y);
		    	mWindowManager.addView(mTv, mParams);
			}
    	});
	}
	
	public String getText() {
		return mTv.getText().toString();
	}
	
	private void setFocusable(boolean value) {
		if (DEBUG) Log.i(TAG, mTag + ": setting focusable: " + value);
		int newFlags;
		int currentFlags = mParams.flags;
		if (value) {
			// Make it focusable, clear the FLAG_ALT_FOCUSIBLE_IM too
			newFlags = currentFlags & ~(
					WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM |
					WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
			
		} else {
			// Make it not focusable, but still hidden behind
			// the IME when it pops up
			newFlags = currentFlags |
					WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM |
					WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
		}
		mParams.flags = newFlags;
		mWindowManager.updateViewLayout(mTv, mParams);
	}
	
	private class WindowViewTouchListener implements View.OnTouchListener {

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			if (DEBUG) Log.i(TAG, mTag + ": Motion Event: " + event);
			if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
				mListener.onWindowUnfocus(SecureInputWindow.this);
				setFocusable(false);
			} else {
				mListener.onWindowFocus(SecureInputWindow.this);
				setFocusable(true);
			}
			return false;
		}
		
	}
	
	private class WindowTextViewWatcher implements TextWatcher {

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {}

		@Override
		public void afterTextChanged(Editable s) {
			mListener.onContentChange(SecureInputWindow.this);
		}
		
	}
	
	private class WindowKeyListener implements OnKeyListener {

		@Override
		public boolean onKey(View v, int keyCode, KeyEvent event) {
			if (keyCode == KeyEvent.KEYCODE_ENTER) {
				// ` then thats a hide keyboard
				mListener.onInputFinish(SecureInputWindow.this);
			}
			return false;
		}
		
	}
	
	private WindowManager.LayoutParams getInitialLayoutParams() {
		WindowManager.LayoutParams p = new WindowManager.LayoutParams( 
	    		mWidth,
	    		WindowManager.LayoutParams.WRAP_CONTENT,

	    		WindowManager.LayoutParams.TYPE_APPLICATION_PANEL,

	    		WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |
	    		WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH |
	    		WindowManager.LayoutParams.FLAG_SECURE,

	    		PixelFormat.TRANSLUCENT
	    	);
		p.gravity = Gravity.TOP | Gravity.LEFT;
		
		// Initially unfocusable
		p.flags |= (WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM |
					WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
		return p;
	}
}
