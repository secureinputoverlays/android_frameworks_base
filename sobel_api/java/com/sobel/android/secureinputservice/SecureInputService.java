package com.sobel.android.secureinputservice;

import java.util.concurrent.ConcurrentHashMap;

import com.sobel.android.hiddenbufferservice.HiddenBufferHandle;
import com.sobel.android.hiddenbufferservice.HiddenBufferManager;

import android.app.ActivityThread;
import android.content.Context;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

/**
 * The SecureInputService
 * 
 * @hide
 */
public class SecureInputService extends ISecureInputService.Stub
implements SecureInputWindowListener {

	private static final String TAG = "SecureInputService";
	
	private HandlerThread mHandlerThread;
	private Handler mHandler;

	private HiddenBufferManager mHiddenBufferManager;
	private ConcurrentHashMap<IBinder, IBinder> mBufferTokensByWindowId;
	private ConcurrentHashMap<IBinder, ISecureInputServiceListener> mListenersByWindowId;
	
	public SecureInputService() {
		mHandlerThread = new HandlerThread(TAG, 
				android.os.Process.THREAD_PRIORITY_FOREGROUND);
		mHandlerThread.start();
		mHandler = new Handler(mHandlerThread.getLooper(), null, true);

		mHiddenBufferManager = new HiddenBufferManager();
		mBufferTokensByWindowId = new ConcurrentHashMap<IBinder, IBinder>();
		mListenersByWindowId = new ConcurrentHashMap<IBinder, ISecureInputServiceListener>();
	}
	
	@Override
	public SecureInputHandle addWindow(String contents, IBinder token, int w, int x, int y, int inputType, ISecureInputServiceListener listener) {
		Context c = ActivityThread.currentActivityThread().getSystemContext();
		SecureInputWindow siw = new SecureInputWindow(w, c, mHandler, token, contents, inputType, this, "HBS:1");

		// Stash calling identity so we can use system-only HBS API
		long caller = Binder.clearCallingIdentity();
		HiddenBufferHandle hbsHandle = mHiddenBufferManager.newBuffer();
		Binder.restoreCallingIdentity(caller);

		// Stash the privateToken pointed to by the window id,
		// The public portion will be returned to caller
		mBufferTokensByWindowId.put(siw.windowId, hbsHandle.mPrivateToken);

		// Add a reference back to our listener
		mListenersByWindowId.put(siw.windowId, listener);
		
		// Show it
		siw.show(x, y);

		// Prepare and return the response
		SecureInputHandle h = new SecureInputHandle(hbsHandle.mPublicToken, siw.windowId);
		return h;
    }

	@Override
	public void onWindowFocus(SecureInputWindow w) {
		Log.v(TAG, String.format("Window %s got focus", w.windowId));
		ISecureInputServiceListener l = mListenersByWindowId.get(w.windowId);
		if (l == null) {
			Log.v(TAG, String.format("No listener for window %s!", w.windowId));
		} else {
			try {
				l.onEditStart(w.windowId);
			} catch (RemoteException e) {
				throw new RuntimeException("Error making edit start callback");
			}
		}
	}

	@Override
	public void onWindowUnfocus(SecureInputWindow w) {
		Log.v(TAG, String.format("Window %s lost focus", w.windowId));
		ISecureInputServiceListener l = mListenersByWindowId.get(w.windowId);
		if (l == null) {
			Log.v(TAG, String.format("No listener for window %s!", w.windowId));
		} else {
			try {
				l.onEditEnd(w.windowId);
			} catch (RemoteException e) {
				throw new RuntimeException("Error making edit end callback");
			}
		}
	}

	@Override
	public void onContentChange(SecureInputWindow w) {
		Log.v(TAG, String.format("Window %s text change: %s", w.windowId, w.getText()));
		IBinder token = mBufferTokensByWindowId.get(w.windowId);
		if (token == null) {
			Log.e(TAG, String.format("No buffer token associated with window %s!", w.windowId));
			return;
		}
		mHiddenBufferManager.setBufferText(token, w.getText());
	}

	@Override
	public void onInputFinish(SecureInputWindow w) {
		Log.v(TAG, String.format("Window %s input finished", w.windowId));
		ISecureInputServiceListener l = mListenersByWindowId.get(w.windowId);
		if (l == null) {
			Log.v(TAG, String.format("No listener for window %s!", w.windowId));
		} else {
			try {
				l.onEditEnd(w.windowId);
			} catch (RemoteException e) {
				throw new RuntimeException("Error making edit end callback");
			}
		}
	}
}
