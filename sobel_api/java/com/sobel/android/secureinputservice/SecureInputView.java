package com.sobel.android.secureinputservice;
/**
 * Public Facing Api for secure inputs
 * 
 * Some inspiration from layer_cake EmbeddedActivityView
 * https://layercake.cs.washington.edu/
 * 
 */

import com.sobel.android.hiddenbufferservice.HiddenBufferManager;
import com.sobel.android.hiddenbufferservice.HiddenContentHandle;
import com.sobel.android.secureinputservice.SecureInputManager;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.IBinder;
import android.text.InputType;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

public class SecureInputView extends LinearLayout {

	private static final String TAG = "SecureInputView";
	
	private SecureInputManager mSecureInputManager;
	private HiddenBufferManager mHiddenBufferManager;
	
	private EditText mShadowEditText;
	private boolean mWindowCreated;
	
	private IBinder mHiddenBufferToken;
	private IBinder mSecureInputToken;
	
	private SecureInputViewListener mDelegate;
	
	private int mInputType;
	
	public SecureInputView(Context context) {
		super(context);
		init(context);
	}
	
	public SecureInputView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}
	
	public SecureInputView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		
		TypedArray a = context.obtainStyledAttributes(attrs, com.android.internal.R.styleable.TextView, defStyle, 0);
        
        final int N = a.getIndexCount();
        for (int i = 0; i < N; ++i)
        {
            int attr = a.getIndex(i);
            switch (attr)
            {
                case com.android.internal.R.styleable.TextView_inputType:
                    mInputType = a.getInt(attr, InputType.TYPE_CLASS_TEXT);
                    Log.v(TAG, String.format("Read input type: %x", mInputType));
                    break;
            }
        }
        a.recycle();  

		init(context);
	}
	
	private void init(Context context) {
		mSecureInputManager = (SecureInputManager)context.getSystemService(Context.SECURE_INPUT_SERVICE);
		mHiddenBufferManager = new HiddenBufferManager();
		
		mShadowEditText = new EditText(context);
		this.addView(mShadowEditText);

		// Force redraw at some point...
		this.setBackgroundColor(Color.WHITE);
	}

	public void setListener(SecureInputViewListener l) {
		mDelegate = l;
	}
	
	public HiddenContentHandle getHiddenContentHandle() {
		if (mHiddenBufferToken == null) {
			// Happens if this is called before initWindow
			throw new RuntimeException("getHiddenContentHandle must be called after init window");
		} else {
			// Get a snapshot token
			IBinder snapshotToken = mHiddenBufferManager.createSnapshot(mHiddenBufferToken);
			return new HiddenContentHandle(snapshotToken);
		}
	}
	
    @Override
    protected void onDraw(Canvas canvas) {
    	super.onDraw(canvas);
    	if (!mWindowCreated) {
    		initWindow();
    	} else {
    		maybeRefreshWindow();
    	}
    }
   
	private void maybeRefreshWindow() {
		Log.v(TAG, "maybreRefresh" + debugParams());
	}

	private void initWindow() {
		
		mShadowEditText.setVisibility(View.INVISIBLE);
		ViewGroup.LayoutParams slp = mShadowEditText.getLayoutParams();
		slp.width = LayoutParams.MATCH_PARENT;
		mShadowEditText.setLayoutParams(slp);
		
		ViewGroup.LayoutParams lp = this.getLayoutParams();
		//lp.width = LayoutParams.MATCH_PARENT;
		lp.height = LayoutParams.WRAP_CONTENT;
		this.setLayoutParams(lp);

		int[] loc = new int[2]; 
    	this.getLocationOnScreen(loc);
    	int x = loc[0];
    	int y = loc[1];
		
    	int w = this.getWidth();
    	
		SecureInputHandle h = mSecureInputManager.addWindow("", this.getApplicationWindowToken(), w, x, y, mInputType, this);
		mHiddenBufferToken = h.mHiddenBufferToken;
		mSecureInputToken = h.mSecureInputToken;
		
		Log.v(TAG, "created " + debugParams());
		mWindowCreated = true;
	}
	
	private String debugParams() {
		int[] loc = new int[2]; 
    	this.getLocationOnScreen(loc);
    	int x = loc[0];
    	int y = loc[1];
    	
    	int h = this.getHeight();
    	int w = this.getWidth();
    	
    	return String.format("[x:%d,y:%d,h:%d,w:%d]", x, y, h, w);
	}

    /* Event Handlers from SecureInputManager */

	/**
	 * @hide 
	 */
	public void onEditStart() {
		if (mDelegate != null) {
			mDelegate.onEditStart();
		}
    	Log.v(TAG, "editStart");
    }
	
	/**
	 * @hide
	 */
	public void onEditStop() {
		if (mDelegate != null) {
			mDelegate.onEditStop();
		}
		Log.v(TAG, "editStop");
	}
	
}
