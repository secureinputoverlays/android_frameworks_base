package com.sobel.android.secureinputservice;

/**
 * @hide
 */
public interface SecureInputWindowListener {

	public void onWindowFocus(SecureInputWindow w);
	public void onWindowUnfocus(SecureInputWindow w);
	public void onContentChange(SecureInputWindow w);
	public void onInputFinish(SecureInputWindow w);
	
}
