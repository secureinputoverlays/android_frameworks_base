package com.sobel.android.secureinputservice;

import android.os.IBinder;

import com.sobel.android.secureinputservice.SecureInputHandle;

oneway interface ISecureInputServiceListener {

	void onEditStart(IBinder windowId);
	void onEditEnd(IBinder windowId);

}
