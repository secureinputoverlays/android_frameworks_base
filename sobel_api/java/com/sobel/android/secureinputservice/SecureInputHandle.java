package com.sobel.android.secureinputservice;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;

/** Passed from the secure input service
 *  back to the secure input manager
 *  
 *  includes the public token for the HiddenBufferService
 *  and a token to access the secure input window
 *  
 *  @hide
 */
public class SecureInputHandle implements Parcelable {

	public final IBinder mHiddenBufferToken;
	public final IBinder mSecureInputToken; 
	
	@Override
	public int describeContents() {
		return 0;
	}
	
	public SecureInputHandle(IBinder hiddenBufferToken, IBinder secureInputToken) {
		mHiddenBufferToken = hiddenBufferToken;
		mSecureInputToken = secureInputToken;
	}

	public SecureInputHandle(Parcel in) {
		mHiddenBufferToken = in.readStrongBinder();
		mSecureInputToken = in.readStrongBinder();
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeStrongBinder(mHiddenBufferToken);
		dest.writeStrongBinder(mSecureInputToken);
	}
	
    public static final Parcelable.Creator<SecureInputHandle> CREATOR
    = new Parcelable.Creator<SecureInputHandle>() {
        public SecureInputHandle createFromParcel(Parcel in) {
            return new SecureInputHandle(in);
        }

        public SecureInputHandle[] newArray(int size) {
            return new SecureInputHandle[size];
        }
    };

    @Override
    public String toString() {
    	return String.format("SecureInputHandle<hbs:%s,sit:%s>", mHiddenBufferToken, mSecureInputToken);
    }

}
