package com.sobel.android.attestedhttpservice;

import android.os.IBinder;

import com.sobel.android.attestedhttpservice.IAttestedPostListener;

/**
 * @hide
 */
interface IAttestedHttpService {

	void performPOST(String url, in Map unsafeHeaders, in Map unsafeParams, IAttestedPostListener listener);

}
