package com.sobel.android.attestedhttpservice;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.android.okhttp.MediaType;
import com.android.okhttp.MultipartBuilder;
import com.android.okhttp.OkHttpClient;
import com.android.okhttp.Request;
import com.android.okhttp.Response;
import com.android.okio.ByteString;
import com.android.okio.OkBuffer;
import com.sobel.android.hiddenbufferservice.HiddenBufferManager;
import com.sobel.android.tpmsigningservice.TPMSigningClient;

import android.os.IBinder;
import android.os.RemoteException;
import android.util.Base64;
import android.util.Log;

/**
 * @hide
 */
public class AttestedPostRequest implements Runnable {

	private final String TAG;
	
	private static class RequestException extends Exception {
		public RequestException(String s) {
			super(s);
		}
	}
	
	// Conflict. Taking cue from Kannan and Dropbox API.
	public static int DONT_LOCKDOWN_BUFFER_CODE = 409;
	
	private static final String ATTESTATION_SIGNATURE_HEADER = "X-Attestation-Signature";
	
	// HTTP client passed in from service
	private OkHttpClient mClient;
	
	private URL mUrl;
	private HashMap<String, String> mHeaders;
	private HashMap<String, AttestedPostParam> mParams;
	
	private boolean mShouldLockdownBuffersOnError;

	private IAttestedPostListener mListener;
	
	public AttestedPostRequest(OkHttpClient client, String id, URL url,
			HashMap<String, String> headers, HashMap<String, AttestedPostParam> params,
			IAttestedPostListener listener) {
		mClient = client;

		TAG = "AttestedPostRequest>" + id;

		mUrl = url;
		mHeaders = headers;
		mParams = params;
		
		// Initially false, not true until outgoing.
		mShouldLockdownBuffersOnError = false;
		
		mListener = listener;
	}

	@Override
	public void run() {
		Log.v(TAG, "Request to: " + mUrl);
		Iterator<String> it;
		it = mHeaders.keySet().iterator();
		while (it.hasNext()) {
			String s = it.next();
			Log.v(TAG, s + ": " + mHeaders.get(s));
		}
		Log.v(TAG, "------");
		it = mParams.keySet().iterator();
		while (it.hasNext()) {
			String s = it.next();
			Log.v(TAG, s + "=" + mParams.get(s));
		}
		
		Response r;
		try {
			byte[] nonce = getNonce();
			Request.Body content = getContent(nonce);
			byte[] signature = getSignature(nonce, content);
			r = doPost(content, signature);
			sendResponseToListener(r);
			// And we're done!
			Log.v(TAG, "Request completed successfully.");
			return;
			
		} catch (RequestException e) {
			resetExfiltrationState(mShouldLockdownBuffersOnError);
			Log.v(TAG, "RequestException: " + e.getMessage());
			try {
				mListener.onError(e.getMessage());
			} catch (RemoteException e1) {
				// Whatever....
				Log.v(TAG, "RemoteException reporting RequestException to listener");
			}
			return;
		}
	}
	
	/**
	 * Get nonce url by appending "nonce"
	 * to whatever current path is.
	 */
	private URL getNonceUrl() throws RequestException {
		URL nonceUrl;
		String currentPath = mUrl.getPath();
		// If currentPath is present, it will always start with "/"

		// Maybe append a slash
		if (!currentPath.endsWith("/")) {
			currentPath += "/";
		}
		// And now "nonce"
		String noncePath = currentPath + "nonce";
		try {
			nonceUrl = new URL(mUrl, noncePath);  // Yikes...
		} catch (MalformedURLException e) {
			throw new RequestException("Malformed nonce URL: " + e.getMessage());
		}
		return nonceUrl;
	}
	
	/**
	 * Does a GET to mURL/nonce
	 * A 200 response implies the body is the nonce.
	 */
	private byte[] getNonce() throws RequestException {
		URL nonceUrl = getNonceUrl();

		Request req = new Request.Builder()
			.get()
			.url(nonceUrl)
			.build();
		
		Response res;
		try {
			res = mClient.execute(req);
		} catch (IOException e) {
			throw new RequestException("IOException requesting nonce: " + e.getMessage());
		}

		if (res.code() != 200) {
			throw new RequestException("Response code from nonce is not 200: " + res.code());
		}

		byte[] data;
		try {
			data = res.body().bytes();
		} catch (IOException e) {
			throw new RequestException("IOException reading nonce request body: " + e.getMessage());
		}
		
		Log.v(TAG, String.format("Got nonce %s from %s",
				Base64.encodeToString(data, Base64.DEFAULT), nonceUrl.toString()));
		return data;
	}
		
	private String logKeyForContentKey(String s) {
		return s + "-query-log";
	}
	
	private void staticParamNameCheck() throws RequestException {
		// Does static checks on the names of parameters
		// - none ending with -query-log
		// - none `nonce` or `exfiltration-url`
		Iterator<String> it = mParams.keySet().iterator();
		while (it.hasNext()) {
			String key = it.next();
			if (key.endsWith("-query-log")) {
				throw new RequestException("Parameter names cannot end with '-query-log': " + key);
			}
			if (key.equals("nonce")) {
				throw new RequestException("'nonce' is a reserved parameter name");
			}
			if (key.equals("exfiltration-url")) {
				throw new RequestException("'exfiltration-url' is a reserved parameter name");
			}
		}
	}
	
	private Request.Body getContent(byte[] nonce) throws RequestException {
		HiddenBufferManager hbm = new HiddenBufferManager();

		staticParamNameCheck();
		
		// Multipart request body
		MultipartBuilder b = new MultipartBuilder()
			.type(MultipartBuilder.FORM)
			.addFormDataPart("nonce", Base64.encodeToString(nonce, Base64.DEFAULT))
			.addFormDataPart("exfiltration-url", mUrl.toString());
		
		Iterator<String> it = mParams.keySet().iterator();
		while (it.hasNext()) {
			String k = it.next();
			AttestedPostParam p = mParams.get(k);
			switch (p.getParamType()) {
			case AttestedPostParam.STRING:
				b.addFormDataPart(k, p.getStringValue());
				break;
			case AttestedPostParam.HIDDEN_BUFFER:
				IBinder snapshotToken = p.getSnapshotTokenValue();
				String contents = hbm.getContents(snapshotToken);
				if (contents == null) {
					throw new RequestException("No contents assocated with given snapshot token");
				}

				// Now testAndSet target, this will also have benefit of locking down queries
				boolean exfiltrationTargetOk = hbm.testAndSetExfiltrationTarget(snapshotToken, mUrl.toString());
				if (!exfiltrationTargetOk) {
					throw new RequestException("Cannot set exfiltrationTarget, already sent somewhere?");
				}

				// Get log after contents to be as up to date as possible
				byte[] log = hbm.getLog(snapshotToken);
				if (log == null) {
					throw new RequestException("No log assocated with given snapshot token");
				}
				String logKey = logKeyForContentKey(k);
				b.addFormDataPart(k, contents);

				// Ensures no collisions between existing param names and
				// ones we'll create for param logs
				if (mParams.containsKey(logKey)) {
					throw new RequestException("Cannot have param name of log for hidden buffer " + k + " logKey: " + logKey);
				}
				b.addFormDataPart(logKey, Base64.encodeToString(log, Base64.DEFAULT));
				break;
			default:
				throw new RequestException("Unknown AttestedPostParam type: " + p.getParamType());
			}
		}
		return b.build();
	}

	private byte[] getSignature(byte[] nonce, Request.Body content) throws RequestException {
    	TPMSigningClient t = new TPMSigningClient();

    	OkBuffer contentBuffer = new OkBuffer();
    	try {
			content.writeTo(contentBuffer);
		} catch (IOException e) {
			throw new RequestException("Exception writing content to buffer: " + e.getMessage());
		}
    	ByteString contentByteString = contentBuffer.readByteString(contentBuffer.size());
    	byte[] data = contentByteString.toByteArray();
    	return t.quoteData(data, nonce);
	}

	private Response doPost(Request.Body content, byte[] signature) throws RequestException {
		Request.Builder rb = new Request.Builder()
				.post(content)
				.url(mUrl);

		// Check for collisions with our header and add all user headers
		Iterator<String> it = mHeaders.keySet().iterator();
		while (it.hasNext()) {
			String h = it.next();
			if (h.equalsIgnoreCase(ATTESTATION_SIGNATURE_HEADER)) {
				throw new RequestException(ATTESTATION_SIGNATURE_HEADER + " is a reserved header");
			}
			rb.addHeader(h, mHeaders.get(h));
		}

		// Add our header
		// NO_WRAP is important, otherwise HTTP breaks
		rb.addHeader("X-Attestation-Signature", Base64.encodeToString(signature, Base64.NO_WRAP));

		// Now this is True -- the next error would be a web request,
		// And we can't know whether server thinks it receives the packet
		// So after this point, from a lockdown perspective, RequestException
		// treated like a 500.
		mShouldLockdownBuffersOnError = true;
		
		// Build and execute
		Request req = rb.build();
		try {
			return mClient.execute(req);
		} catch (IOException e) {
			throw new RequestException("IOException performing post: " + e.getMessage());
		}
	}
	
	private Map<String, String> extractResponseHeaders(Response r) {
		// Headers is a little tricky - headers is really a multidict,
		// but we just pull out the last one :/ 
		HashMap<String, String> responseHeaders = new HashMap<String, String>();
		Set<String> responseHeaderNames = r.headers().names();
		Iterator<String> it = responseHeaderNames.iterator();
		while (it.hasNext()) {
			String responseHeader = it.next();
			List<String> values = r.headers().values(responseHeader);
			// Check just to be sure
			if (!values.isEmpty()) {
				responseHeaders.put(responseHeader, values.get(values.size() - 1));
			}
		}
		return responseHeaders;
	}
	
	/**
	 * Resets exfiltration state on all snapshots
	 */
	private void resetExfiltrationState(boolean success) {
		HiddenBufferManager hbm = new HiddenBufferManager();

		// Iterate through our params..
		Iterator<String> it = mParams.keySet().iterator();
		while (it.hasNext()) {
			String k = it.next();
			AttestedPostParam p = mParams.get(k);
			if (p.getParamType() == AttestedPostParam.HIDDEN_BUFFER) {
				IBinder snapshotToken = p.getSnapshotTokenValue();
				boolean ok = hbm.exfiltrationAttemptFinished(snapshotToken, success);
				if (!ok) Log.v(TAG, "Unable to reenable queries for " + snapshotToken);
			}
		}
	}

	private void sendResponseToListener(Response r) throws RequestException {
		// Hurray the request succeeded. 
		// Get information back to listener.
		int code = r.code();
		String statusLine = r.statusLine();
		Map<String, String> responseHeaders = extractResponseHeaders(r);
	
		byte[] responseBody;
		try {
			responseBody = r.body().bytes();
		} catch (IOException e) {
			// Ack. so close.
			throw new RequestException("IOException reading POST response body: " + e.getMessage());
		}
		
		// reset exfiltration
		// Lock it down no matter what!
		resetExfiltrationState(true);
		
		try {
			mListener.onSuccess(code, statusLine, responseHeaders, responseBody);
		} catch (RemoteException e) {
			// Sucks for them.
			// Also a little for us - because we don't know how this failed:
			// - before it got to them
			// - after it got to them
			// So the invariant of mutual exclusion between onError and onSuccess
			// may end up violated...
			// Ah well.
			throw new RequestException("RemoteException sending response to listener..." + e.getMessage());
		}
	}
}
