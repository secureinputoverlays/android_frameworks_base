package com.sobel.android.attestedhttpservice;

/**
 * @hide
 */
oneway interface IAttestedPostListener {

	void onSuccess(int code, String statusLine, in Map unsafeHeaders, in byte[] body);
	void onError(String msg);

}
