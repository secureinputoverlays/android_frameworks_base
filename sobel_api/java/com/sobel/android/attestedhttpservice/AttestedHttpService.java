package com.sobel.android.attestedhttpservice;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.android.okhttp.OkHttpClient;

import android.Manifest;
import android.app.ActivityThread;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

/**
 * @hide
 */
public class AttestedHttpService extends IAttestedHttpService.Stub {

	public static final String TAG = "AttestedHttpService";
	
	// As described in okhttp docs, can use one of these
	// for all our requests, once configured;
	private OkHttpClient mClient;
	
	private int counter;
	
	public AttestedHttpService() {
		// To give requests unique ids
		counter = 0;
		
		// Do any initialization of the client
		mClient = new OkHttpClient();
	}
	
	@Override
	public void performPOST(String url, Map unsafeHeaders, Map unsafeParams,
			IAttestedPostListener listener) throws RemoteException {		

		// Check for INTERNET permission right away
		Context c = ActivityThread.currentActivityThread().getSystemContext();
		int ok = c.checkCallingPermission(Manifest.permission.INTERNET);
		if (ok == PackageManager.PERMISSION_DENIED) {
			// Well glad we know that.
			listener.onError("Permission Denied: Application must have INTERNET permission");
			Log.v(TAG, "Permission Denied!");
			return;
		}
		
		// Straight away lets get type safety back
		HashMap<String, String> headers;
		HashMap<String, AttestedPostParam> params;
		try {
			headers = convertUnsafeHeaders(unsafeHeaders);
			params = convertUnsafeParams(unsafeParams);
		} catch (IllegalArgumentException e) {
			listener.onError("IllegalArgument: " + e.getMessage());
			Log.v(TAG, "Got illegal arugment: " + e.getMessage());
			return;
		}
		
		URL pUrl;
		try {
			pUrl = new URL(url);
		} catch (MalformedURLException e) {
			// TODO: handle this...
			listener.onError("Malformed URL: " + url);
			return;
		}
		
		// Spin up a thread who will
		// drive this request to completion (or error)..
		counter++;
		AttestedPostRequest r = new AttestedPostRequest(mClient, counter + "", pUrl, headers, params, listener);
		Thread t = new Thread(r);
		t.start();
	}

	@SuppressWarnings("rawtypes")
	/**
	 * @hide
	 */
	public static HashMap<String, String> convertUnsafeHeaders(Map unsafeHeaders) {
		// Input argument is untyped,
		// So go through and type check this all.
		HashMap<String, String> out = new HashMap<String, String>();
		Iterator it = unsafeHeaders.keySet().iterator();
		while (it.hasNext()) {
			Object k = it.next();
			if (!(k instanceof String)) {
				throw new IllegalArgumentException("All header keys must be String");
			}
			Object v = unsafeHeaders.get(k);
			if (!(v instanceof String)) {
				throw new IllegalArgumentException("All header values must be String");
			}
			out.put((String)k, (String)v);
		}
		return out;
	}
	
	@SuppressWarnings("rawtypes")
	/**
	 * @hide
	 */
	public static HashMap<String, AttestedPostParam> convertUnsafeParams(Map unsafeHeaders) {
		// Input argument is untyped,
		// So go through and type check this all.
		HashMap<String, AttestedPostParam> out = new HashMap<String, AttestedPostParam>();
		Iterator it = unsafeHeaders.keySet().iterator();
		while (it.hasNext()) {
			Object k = it.next();
			if (!(k instanceof String)) {
				throw new IllegalArgumentException("All param keys must be String");
			}
			Object v = unsafeHeaders.get(k);
			if (!(v instanceof AttestedPostParam)) {
				throw new IllegalArgumentException("All param values must be AttestedPostParam");
			}
			out.put((String)k, (AttestedPostParam)v);
		}
		return out;
	}


}
