package com.sobel.android.attestedhttpservice;

import java.util.HashMap;
import java.util.Map;

import com.sobel.android.hiddenbufferservice.HiddenContentHandle;

import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.os.ServiceManager;

/**
 * Interface to AttestedHttpService
 * @author sobel
 *
 */
public class AttestedPost extends IAttestedPostListener.Stub {

	IAttestedHttpService mAttestedHttpService;
	private Handler mHandler;
	
	private String mUrl;
	private HashMap<String, String> mHeaders;
	private HashMap<String, AttestedPostParam> mParams;
	
	private ResponseListener mResponseListener;
	private ErrorListener mErrorListener;
	
	private Boolean gotResponse = Boolean.FALSE;
	
	/**
	 * Constructor - Finds the AttestedHttpService
	 * 
	 * @hide
	 */
	private AttestedPost(String url, HashMap<String, String> headers, HashMap<String, AttestedPostParam> params) {
		IBinder ahsBinder = ServiceManager.getService("AttestedHTTP");
        if (ahsBinder == null) {
        	throw new RuntimeException("No Such Service 'AttestedHTTP'");
        } else {
        	mAttestedHttpService = IAttestedHttpService.Stub.asInterface(ahsBinder);
        }
        mHandler = new Handler(Looper.getMainLooper());

        mUrl = url;
        mHeaders = headers;
        mParams = params;
	}
	
	public static class Builder {
		private String mUrl;
		private HashMap<String, String> mHeaders;
		private HashMap<String, AttestedPostParam> mParams;

		public Builder() {
			mUrl = null;
			mHeaders = new HashMap<String, String>();
			mParams = new HashMap<String, AttestedPostParam>();
		}

		public Builder url(String s) {
			mUrl = s;
			return this;
		}

		public Builder addHeader(String k, String v) {
			mHeaders.put(k, v);
			return this;
		}
		
		public Builder addParam(String k, String s) {
			mParams.put(k, AttestedPostParam.forString(s));
			return this;
		}
		
		public Builder addParam(String k, HiddenContentHandle h) {
			// Will throw error if given handle has already been freed..
			mParams.put(k, AttestedPostParam.forSnapshotToken(h.getSnapshotToken()));
			return this;
		}

		public AttestedPost build() {
			if (mUrl == null) {
				throw new RuntimeException("URL must be provided to AttestedPost.Builder");
			}
			return new AttestedPost(mUrl, mHeaders, mParams);
		}
	}
	
	/**
	 * Does the post
	 */
	public void perform(ResponseListener res, ErrorListener err) {
		mResponseListener = res;
		mErrorListener = err;
    	try {
    		mAttestedHttpService.performPOST(mUrl, mHeaders, mParams, this);
    	} catch (RemoteException e) {
    		throw new RuntimeException("Error accessing Service: " + e.toString());
    	}
	}

	public interface ResponseListener {
		/**
		 * Will be called back on the main thread
		 * 
		 * Note that this is __transport__ level success
		 * Response code could be a non 2xx
		 */
		public void onResponse(AttestedPostResponse r);
	}
	
	public interface ErrorListener {
		/**
		 * Will be called back on the main thread
		 * 
		 * Note that this is __transport__ level error
		 * Means that things FAILED, not that we got a non
		 * 2xx return code
		 */
		public void onError(String msg);
	}

	/**
	 * @hide 
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void onSuccess(int code, String statusLine, Map unsafeHeaders, byte[] body) throws RemoteException {
		synchronized (gotResponse) {
			if (gotResponse) {
				throw new IllegalStateException("Already got a response!");
			}
			gotResponse = Boolean.TRUE;
		}
		
		HashMap<String, String> headers = AttestedHttpService.convertUnsafeHeaders(unsafeHeaders); 
		final AttestedPostResponse r = new AttestedPostResponse(code, statusLine, headers, body);
		if (mResponseListener != null) {
			mHandler.post(new Runnable() {
				@Override
				public void run() {
					mResponseListener.onResponse(r);
				}
			});
		}
	}

	/**
	 * @hide
	 */
	@Override
	public void onError(String msg) throws RemoteException {
		synchronized (gotResponse) {
			if (gotResponse) {
				throw new IllegalStateException("Already got a response!");
			}
			gotResponse = Boolean.TRUE;
		}
		
		final String eMsg = msg;
		if (mErrorListener != null) {
			mHandler.post(new Runnable() {
				@Override
				public void run() {
					mErrorListener.onError(eMsg);
				}
			});
		}
		
	}

}
