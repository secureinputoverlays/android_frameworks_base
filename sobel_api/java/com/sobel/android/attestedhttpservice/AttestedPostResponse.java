package com.sobel.android.attestedhttpservice;

import java.util.HashMap;

public class AttestedPostResponse {

	private final int mCode;
	private final String mStatusLine;
	private final HashMap<String, String> mHeaders;
	private final byte[] mBody;
	
	public AttestedPostResponse(int code, String statusLine, HashMap<String, String> headers, byte[] body) {
		mCode = code;
		mStatusLine = statusLine;
		mHeaders = headers;
		mBody = body;
	}

	public int getCode() {
		return mCode;
	}

	public String getStatusLine() {
		return mStatusLine;
	}

	public HashMap<String, String> getHeaders() {
		return mHeaders;
	}
	
	public byte[] getBody() {
		return mBody;
	}
	

}
