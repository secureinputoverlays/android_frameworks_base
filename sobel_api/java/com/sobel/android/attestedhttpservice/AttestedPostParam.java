package com.sobel.android.attestedhttpservice;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Tagged Union for possible post key params
 *
 * Internal to AttestedPost
 * @hide
 */
public class AttestedPostParam implements Parcelable {

	/**
	 * Param types
	 */
	public static final int STRING = 0;
	public static final int HIDDEN_BUFFER= 1;
	

	private final int mParamType;
	private final String mStringValue;
	private final IBinder mSnapshotToken;
	
	private AttestedPostParam(int type, String s, IBinder t) {
		mParamType = type;
		mStringValue = s;
		mSnapshotToken = t;
	}
	
	public static AttestedPostParam forString(String s) {
		return new AttestedPostParam(STRING, s, null);
	}
	
	public static AttestedPostParam forSnapshotToken(IBinder t) {
		return new AttestedPostParam(HIDDEN_BUFFER, null, t);
	}
	
	public int getParamType() {
		return mParamType;
	}
	
	public String getStringValue() {
		if (mParamType != STRING) {
			throw new RuntimeException("tagged union access error (STRING)");
		}
		return mStringValue;
	}

	public IBinder getSnapshotTokenValue() {
		if (mParamType != HIDDEN_BUFFER) {
			throw new RuntimeException("tagged union acess error (HIDDEN_BUFFER)");
		}
		return mSnapshotToken;
	}
	
	public String toString() {
		switch (mParamType) {
		case STRING:
			return mStringValue;
		case HIDDEN_BUFFER:
			return mSnapshotToken.toString();
		default:
			return "<unknown_type>";
		}
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(mParamType);
		dest.writeString(mStringValue);
		dest.writeStrongBinder(mSnapshotToken);
	}
	
	public static final Parcelable.Creator<AttestedPostParam> CREATOR
	= new Parcelable.Creator<AttestedPostParam>() {
	    public AttestedPostParam createFromParcel(Parcel in) {
	        int paramType = in.readInt();
	        String s = in.readString();
	        IBinder t = in.readStrongBinder();
	        return new AttestedPostParam(paramType, s, t);
	    }

	    public AttestedPostParam[] newArray(int size) {
	        return new AttestedPostParam[size];
	    }
	};
}
