"""
Uses FakeTPMAIK.java.jinja template
and fills it in

requires pyrsa and jinja2
"""
import rsa
import jinja2

BITS = 2048
TEMPLATE_FILE = "FakeTPMAIK.java.jinja"
OUTPUT_FILE = "FakeTPMAIK.java"

with open(TEMPLATE_FILE) as tf:
    template = jinja2.Template(tf.read())

_, private_key = rsa.newkeys(2048)

header_comment = """
/**
 * DO NOT EDIT
 * Auto generated by gen_fake_tpm_aik.py
 */
"""

tvs = {
    'header_comment': header_comment,
    'modulus': private_key.n,
    'publicExponent': private_key.e,
    'privateExponent': private_key.d,
    'prime1': private_key.p,
    'prime2': private_key.q,
    'exponent1': private_key.exp1,
    'exponent2': private_key.exp2,
    'coefficient': private_key.coef,
}

with open(OUTPUT_FILE, 'w') as of:
    of.write(template.render(**tvs))
