"""
Sanity check for TPMSign implementation

by reimplementing it in python without looking :/
"""
import struct
import hashlib
import base64

import rsa

public_exponent = 65537
modulus = 19861483253324017464203675899876180134619591275875120738762748426314962258024858715013457896620544058952938545480646860203441541020332549850042367575130241872893673786193124038788419496756710902226394020337186132192057903970778214011961962997247971098187898049385256157764149736473331412485773070811141331678899790297442103565205266551493909933555248659140731474919599421494780181283830152208085950711435458482940602861311293132510718006714547371917111911859419267249029495537794488402565266943382660375484907237384930267064574492248352866733580816244976001318970041603358214458316156347747792722349968114307113694151

signatureb64 = "jJVNmW2fokGUChwSiwMaFkjcm1LOF/f7dO1kx8eWHPiYiYVgeLMYbqE4qsfb/uTBFQRB+KNAiuzTj2rznf0JzemrYp+6qTZur58Lo5s+VhiG1qKiypF8pGSHzSmNBe++CwrXgZpr9t/XrPHOlFMntU9Mkw9I/mlDcBK/pm35+jZyG9y0sXZg8efWE7lYpZnSDzdobROfzXknkQiHWb37QOuajxbxDNFkduPc4mC0h8a0Bcm5A/npEkDTsFXQyaY5+VEDEPClsPmaCkWnidqsEohOPH9MfLvQBTVJgavlegRJSr6/1KCdA1zcSJV2R6bwGjnvWDpTJdT+WvY2RnzwNg=="
signature = base64.b64decode(signatureb64)

data = "\x00\x01\x02\x03\x04\x05"
dataDigest = hashlib.sha1(data).digest()

nonce = 123456789
nonceDigest = hashlib.sha1(struct.pack("> I", nonce)).digest()

print repr(dataDigest), repr(nonceDigest)

pk = rsa.PublicKey(modulus, public_exponent)

# Pcr Selection
pcr_selection = struct.pack(">H B B B", 3, 0x00, 0x00, 0x80)

print "PCR Selection:", len(pcr_selection), repr(pcr_selection)

pcr23 = hashlib.sha1('\x00'*20 + dataDigest).digest()
pcr_composite = struct.pack(">5s I 20s", pcr_selection, 20, pcr23)


print "PCR Composite:", len(pcr_composite), repr(pcr_composite)

composite_hash = hashlib.sha1(pcr_composite).digest()

version = 0x01010000
fixed = 'QUOT'

quote_info = struct.pack(">I 4s 20s 20s", version, fixed, composite_hash, nonceDigest)

rsa.verify(quote_info, signature, pk)

print "hurray - it passes"