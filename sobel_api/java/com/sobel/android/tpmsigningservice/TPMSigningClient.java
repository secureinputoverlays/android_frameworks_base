package com.sobel.android.tpmsigningservice;

import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;


/**
 * TODO: hide this, but leave it public to debug against I guess :/
 */
public class TPMSigningClient {

	private static final String TAG = "TPMSigningClient";
	
	private final ITPMSigningService mTPMSigningService;

	public TPMSigningClient() {
		IBinder b = ServiceManager.getService("tpm_signing");
        if (b == null) {
        	throw new RuntimeException("No Such Service 'tpm_signing'");
        }
        mTPMSigningService = ITPMSigningService.Stub.asInterface(b);
	}

	public byte[] quoteData(byte[] data, byte[] nonce) {
		try {
			Log.v(TAG, "quoteData");
			return mTPMSigningService.quoteData(data, nonce);
		} catch (RemoteException e) {
			throw new RuntimeException("Error accessing Service: " + e.toString());
		}
	}	
}
