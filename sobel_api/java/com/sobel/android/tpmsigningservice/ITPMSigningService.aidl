package com.sobel.android.tpmsigningservice;

/**
 * @hide
 */
interface ITPMSigningService {
	// Returns the signature of a TPM_QUOTE_INFO
	// with PCR23 after extending the digest of
	// data into that PCR.
	// The digest of nonce is used as the nonce
	byte[] quoteData(in byte[] data, in byte[] nonce);
}
