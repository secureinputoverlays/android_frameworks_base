package com.sobel.android.tpmsigningservice;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.android.org.bouncycastle.asn1.pkcs.RSAPrivateKey;
import com.android.org.bouncycastle.crypto.CryptoException;
import com.android.org.bouncycastle.crypto.DataLengthException;
import com.android.org.bouncycastle.crypto.Digest;
import com.android.org.bouncycastle.crypto.digests.SHA1Digest;
import com.android.org.bouncycastle.crypto.params.RSAKeyParameters;
import com.android.org.bouncycastle.crypto.signers.RSADigestSigner;

/**
 * 
 * Fake TPM
 *
 * All structures packed Big endian - which is 
 */
public class TPMDriver {

	private static void assertLength20(byte[] in) {
		if (in.length != 20) {
			throw new RuntimeException("20 byte array expected");
		}
	}
	
	/**
	 * UINT16 sizeof selection
	 * byte[] selection
	 * 
	 * Spec Part 2 Page 68
	 */
	private static byte[] packedPCRSelection() {
		int length = 2 + 3;
		ByteBuffer bb = ByteBuffer.allocate(length);
		bb.putShort((short)3);
		bb.put((byte)0x00);
		bb.put((byte)0x00);
		bb.put((byte)0x80); // select just 23
		return bb.array();
	}
	
	/**
	 * typedef struct tdTPM_PCR_COMPOSITE {
	 * 		TPM_PCR_SELECTION select;
	 * 		UINT32 valueSize;
	 * 		[size_is(valueSize)] TPM_PCRVALUE pcrValue[];
	 * } TPM_PCR_COMPOSITE;
	 * 
	 * Spec Part 2 Page 69
	 */
	private static byte[] packedPCRComposite(byte[] pcr23) {
		assertLength20(pcr23);
		
		// PCR_SELECTION is 5
		// valueSize is 4
		// One pcr is 20 bytes;
		int length = 5 + 4 + 20;
		ByteBuffer bb = ByteBuffer.allocate(length);
		bb.put(packedPCRSelection());
		bb.putInt(20); // size of pcrValue array (one pcr)
		bb.put(pcr23);
		return bb.array();
	}
	
	/**
	 * typedef struct tdTPM_QUOTE_INFO{
	 * 		TPM_STRUCT_VER version; (0x01010000) 
	 * 		BYTE fixed[4]; (QUOT)
	 * 		TPM_COMPOSITE_HASH digestValue;
	 * 		TPM_NONCE externalData;
	 * } TPM_QUOTE_INFO; 
	 * @return
	 */
	private static byte[] packedQuoteInfo(byte[] compositeHash, byte[] nonce) {
		assertLength20(compositeHash);
		assertLength20(nonce);

		// version is 4
		// fixed is 4
		// composite hash is 20
		// TPM_NONCE is 20
		int length = 4 + 4 + 20 + 20;
		ByteBuffer bb = ByteBuffer.allocate(length);
		bb.putInt(0x01010000); // version
		bb.put((byte)'Q');
		bb.put((byte)'U');
		bb.put((byte)'O');
		bb.put((byte)'T');
		bb.put(compositeHash);
		bb.put(nonce);
		return bb.array();
	}
	
	public static byte[] getSHA1(byte[] data) {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA-1");
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException("Expected to be able to use SHA-1...");
		}
		return md.digest(data);
	}
	
	public static byte[] tpmQuoteDataQuoteInfoStruct(byte[] data, byte[] nonce) {
		// Given data and nonce, gets the quote info that the TPM
		// would sign by extending the PCR into PCR 23
		byte[] dataDigest = getSHA1(data);
		byte[] nonceDigest = getSHA1(nonce);

		// First 20 bytes are initial value of pcr23 - 0#20
		// Next 20 bytes will be filled with data digest
		byte[] extendData = new byte[40];
		System.arraycopy(dataDigest, 0, extendData, 20, 20);
		byte[] pcr23 = getSHA1(extendData);

		byte[] pcrComposite = packedPCRComposite(pcr23);
		byte[] pcrCompositeDigest = getSHA1(pcrComposite);

		return packedQuoteInfo(pcrCompositeDigest, nonceDigest);
	}
	
	public static byte[] tpmQuoteData(byte[] data, byte[] nonce) {

		// OK. so here's what we __would__ do.
		// - digest the data
		// - reset pcr 23 to 0#20
		// - extend it with the datadigest
		// - get a quote over that
		
		// So get the quoteinfo struct
		// then sign that :/
		byte[] quoteInfo = tpmQuoteDataQuoteInfoStruct(data, nonce);
		
		RSAPrivateKey k = FakeTPMAIK.toRSAPrivateKey();
		Digest d = new SHA1Digest();
		RSADigestSigner signer = new RSADigestSigner(d);
		RSAKeyParameters params = new RSAKeyParameters(true, k.getModulus(), k.getPrivateExponent());
		signer.init(true, params);
		signer.update(quoteInfo, 0, quoteInfo.length);
		try {
			return signer.generateSignature();
		} catch (DataLengthException e) {
			throw new RuntimeException("Data length exception");
		} catch (CryptoException e) {
			throw new RuntimeException("crypto exception");
		}
		
	}
	
}
