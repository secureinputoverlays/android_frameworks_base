package com.sobel.android.tpmsigningservice;

import android.os.Binder;
import android.os.Process;
import android.os.RemoteException;
import android.util.Log;

public class TPMSigningService extends ITPMSigningService.Stub {

	private final static String TAG = "TPMSigningService";
	
	@Override
	public byte[] quoteData(byte[] data, byte[] nonce) throws RemoteException {
        int callingUid = Binder.getCallingUid();
        if (callingUid != Process.SYSTEM_UID) {
            throw new SecurityException("getQuote can only be run by the system");
        }

		byte[] signature = TPMDriver.tpmQuoteData(data, nonce);
		Log.v(TAG, "quote data");
		return signature;
	}

}
